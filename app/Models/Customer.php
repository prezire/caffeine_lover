<?php

namespace App\Models;

use App\Models\User;

use App\Models\CustomerProductsConsumed;

//Customer Profile.
class Customer extends User
{
    protected $table = 'customers';

    //User doesn't need to know it has a Customer profile.
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function productsConsumed()
    {
        return $this->hasMany(CustomerProductsConsumed::class);
    }
}
