<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Models\{Customer, Product};

class CustomerProductsConsumed extends Model
{
    use HasFactory;

    protected $table = 'customer_products_consumed';

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @example:
     * Product::isSafe(collect([Product::first()]))
     * Product::isSafe(collect(Product::all()))
     * 
     * @return bool
     */
    public function scopeIsSafe($query, Collection $consumedProducts, Carbon $date = null): bool
    {
        /* if (!is_null($date)) {
            $date = Carbon::parse($date)->format('y-m');
            $grouped = $consumedProducts->groupBy(function($date){
                return Carbon::parse($date->created_at)->format('y-m');
            });

            $consumedProducts = collect($grouped->all());
        }

        print_r($consumedProducts);
        return true; */

        $totalCaffeineMg = $consumedProducts->sum(function ($product) {
            $totalCaffeineMg = 0;
            if ($product->is_caffeinated) {
                $totalCaffeineMg += $product->totalMgTotalServing();
            }
            return $totalCaffeineMg;
        });
        return $totalCaffeineMg <= 500;
    }
}
