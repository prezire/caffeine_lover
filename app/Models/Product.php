<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'serving_size',
        'description',
        'price',
        'is_caffeinated',
        'uom',
        'caffeine_mg_per_serving',
    ];

    public function totalMgTotalServing(): float
    {
        return (float) $this->caffeine_mg_per_serving * $this->serving_size;
    }
}
