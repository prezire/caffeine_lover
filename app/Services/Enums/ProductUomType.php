<?php namespace App\Services\Enums;

class ProductUomType
{
    public static string $FL_OZ = 'fl oz';
    public static string $OZ = 'oz';
}