<?php

namespace App\Services\Converters;

//UOM = Unit of Measure
final class Uom
{

    public static function ozToFlOz(float $ounce): float
    {
        return $ounce *  0.958611419;
    }

    public static function flOzToOz(float $flOunce): float
    {
        return $flOunce * 1.043175557;
    }
}
