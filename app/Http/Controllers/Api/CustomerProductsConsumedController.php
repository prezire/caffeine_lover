<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCustomerProductsConsumedRequest;
use App\Http\Requests\UpdateCustomerProductsConsumedRequest;
use App\Models\CustomerProductsConsumed;

class CustomerProductsConsumedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return auth()->user()->productsConsumed;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCustomerProductsConsumedRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCustomerProductsConsumedRequest $request)
    {
        $c = CustomerProductsConsumed::create($request->all());
        auth()->user()->productsConsumed()->save($c);
        return $c;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustomerProductsConsumed  $customerProductsConsumed
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerProductsConsumed $customerProductsConsumed)
    {
        return $customerProductsConsumed;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCustomerProductsConsumedRequest  $request
     * @param  \App\Models\CustomerProductsConsumed  $customerProductsConsumed
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerProductsConsumedRequest $request, CustomerProductsConsumed $customerProductsConsumed)
    {
        $customerProductsConsumed->fill($request->json()->all());
        return $customerProductsConsumed->fresh();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustomerProductsConsumed  $customerProductsConsumed
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerProductsConsumed $customerProductsConsumed)
    {
        $customer = auth()->user();
        if($customerProductsConsumed->customer === $customer) {
            return $customerProductsConsumed->delete();
        }
        return false;
    }
}
