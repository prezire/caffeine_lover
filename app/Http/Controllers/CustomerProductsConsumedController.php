<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCustomerProductsConsumedRequest;
use App\Http\Requests\UpdateCustomerProductsConsumedRequest;
use App\Models\CustomerProductsConsumed;

class CustomerProductsConsumedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return view('products.consume');
    }
}
