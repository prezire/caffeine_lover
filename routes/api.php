<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\{CustomerController, ProductController, CustomerProductsConsumedController};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('customers', CustomerController::class);

Route::prefix('products')->group(function(){
    //Update parameters to work with Model Binding.
    Route::resource('', ProductController::class)->parameter('', 'product');
    Route::resource('consume', CustomerProductsConsumedController::class)->parameter('consume', 'customerProductsConsumed');
});