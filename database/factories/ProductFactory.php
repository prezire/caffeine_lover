<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'serving_size' => $this->faker->numberBetween(1, 3),
            'is_caffeinated' => true,
            'caffeine_mg_per_serving' => 250
        ];
    }
}
