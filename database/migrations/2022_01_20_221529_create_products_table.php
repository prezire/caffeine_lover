<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Services\Enums\ProductUomType;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name');
            $table->integer('serving_size')->default(1);

            $table->text('description')->nullable();
            $table->float('price')->default(0);

            $table->boolean('is_caffeinated')->default(true);

            //oz. is a unit of weight. fl oz. is a unit of volume. Both are unit of measure (UOM).
            //Use fl oz since the products in this exercise is liquid.
            $table->enum('uom', [ProductUomType::$OZ, ProductUomType::$FL_OZ])->default(ProductUomType::$FL_OZ);
            $table->float('volume')->default(1);

            $table->float('caffeine_mg_per_serving')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
