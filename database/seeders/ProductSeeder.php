<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use Exception;

use App\Services\Enums\ProductUomType;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            Product::firstOrCreate(['name' => 'Monster Ultra Sunrise', 'description' => 'A refreshing orange beverage that has 75mg of caffeine per serving. Every can has two servings.', 'serving_size' => 2, 'caffeine_mg_per_serving' => 75]);

            Product::firstOrCreate(['name' => 'Black Coffee', 'description' => 'The classic, the average 8oz. serving of black coffee has 95mg of caffeine.', 'serving_size' => 1, 'caffeine_mg_per_serving' => 95, 'uom' => ProductUomType::$OZ]);

            Product::firstOrCreate(['name' => 'Americano', 'description' => 'Sometimes you need to water it down a bit... and in comes the americano with an average of 77mg. of caffeine per serving.', 'caffeine_mg_per_serving' => 77]);

            Product::firstOrCreate(['name' => 'Sugar free NOS', 'description' => 'Another orange delight without the sugar. It has 130mg. per serving and each can has two servings.', 'caffeine_mg_per_serving' => 130, 'serving_size' => 2]);

            Product::firstOrCreate(['name' => '5 Hour Energy', 'description' => 'An amazing shot of get up and go! Each 2 fl. oz. container has 200mg of caffeine to get you going.', 'caffeine_mg_per_serving' => 200, 'uom' => ProductUomType::$FL_OZ, 'volume' => 2]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
