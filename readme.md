### Caffeine Lover
#### v1.0.0.0

---

#### Requirements
* Docker (Mine is currently Docker version 20.10.7, build 20.10.7-0ubuntu5~20.04.2 as of Jan 2022).

---

#### About
* Laravel 8 (with Sail)
* Vue.js

---

#### Usage
```sh
# Download the app.
git clone https://gitlab.com/prezire/caffeine_lover.git

cd caffeine_lover

# Copy .env file and fill-up necessary details.
cp .env.example .env

# Setup the Sail alias (Optional).
alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'

# Start services. Note: Make sure Docker is running.
sail up -d

# Install assets.
sail npm install
sail npm run dev

sail artisan migrate

# Self-handling seed. Contains 5 sample drinks and 1 customer with credentials john@doe.com/password.
sail artisan db:seed

# Tests.
sail artisan test --testsuite=Feature
```

---

#### BE
* TODO:

---

#### FE
* Go to http://localhost, or APP_HOST configured in the .env file
* Both Registration and Login buttons are in the home page
---

#### Docs
* TODO: Generator.