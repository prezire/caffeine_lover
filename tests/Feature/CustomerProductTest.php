<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\{User, Customer, Product, CustomerProductsConsumed};

class CustomerProductTest extends TestCase
{
    use RefreshDatabase;

    //BUG: Relations can't be tested as Unit?? https://github.com/laravel/framework/issues/34384

    public function test_consumables()
    {
        $unsafeProducts = Product::factory()->count(2)->make([
            'is_caffeinated' => true,
            'serving_size' => 2,
            'caffeine_mg_per_serving' => 300,
        ]);
        $this->assertFalse(CustomerProductsConsumed::isSafe(collect($unsafeProducts)));

        $safeProducts = Product::factory()->count(2)->make([
            'is_caffeinated' => true,
            'serving_size' => 2,
            'caffeine_mg_per_serving' => 50,
        ]);
        $this->assertTrue(CustomerProductsConsumed::isSafe(collect($safeProducts)));
    }

    public function test_relations()
    {
        $customer = Customer::factory()->create();
        $customer->user()->associate(User::factory()->create());

        $count = 3;

        Product::factory()->count($count)->create()->each(function ($product) use ($customer) {
            $c = CustomerProductsConsumed::factory()->create();
            $c->product()->associate($product);
            $c->customer()->associate($customer);
            $c->save();
            return $c->refresh();
        });
        $customer->refresh();

        $this->assertCount($count, $customer->productsConsumed);
    }
    /**
     * @return void
     */
    public function test_home_page()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
}
